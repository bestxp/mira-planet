<?php
/**
 *Фаил инициализации приложения
 */
namespace liw;

use liw\core\Router;

/**
 * @const string PATH     корень framework каталога
 * @const bool   DEVELOP  флаг рабочей среды
 * @const string BASEPATH корень сайта
 */
define('PATH', __DIR__ . DIRECTORY_SEPARATOR );
define('DEVELOP', false);
//define('BASEPATH', dirname(__DIR__) . '/site/');

require PATH . '/core/Autoload.php'; //Подключение файла автозагрузки

/**
 * Автозагрузка классов
 */

$loader = new Autoload;
$loader->register();

$app = new Router;