<?php
namespace liw\core\base;

use liw\core\helper\Develop;
use liw\core\Liw;

class Controller
{
    /**
     * Экземплят класса View
     * @var \liw\core\base\View
     */
    public $view;
    
    /**
     * Действие по умолчанию
     * @var string
     */
    public $default_action = 'index';
    
    /**
     * Имя активного в данный момент контроллера в нижнем регистре
     * Определяется в классе liw\core\Router
     * @var $controller
     */
    public static $now = '';

    public $load;

    public $name_model;

    public function __construct()
    {
        $this->view = new View;
    }

    /**
     * Вывод ошибок
     * @param  array $data Массив ошибок
     * @return view
     */
    public function show_error($data)
    {
        $this->view->error($data);
    }

      public function model($model_name)
    {   
        $model_path = '\\liw\\models\\'. ucfirst($model_name);
        $model_name = substr(strrchr(strtolower($model_path), "\\"), 1);

        $this->$model_name = new $model_path;
    }

}
