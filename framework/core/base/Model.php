<?php
namespace liw\core\base;

use liw\core\base\model\BaseModel;
use liw\core\Liw;
use mysqli;

class Model extends BaseModel
{   

    private $mysqli;

    private $select_column = ''; //Поля таблицы 
    
    /**
     * Осуществляет подключение к базе данных
     */
    public function __construct()
    {
        @$this->mysqli = new mysqli(
            Liw::$config['db']['host'],
            Liw::$config['db']['user'],
            Liw::$config['db']['pass'],
            Liw::$config['db']['name']
        );
        
        if ($this->mysqli->connect_error) {
            throw new \Exception(
            	"Connection Error ({$this->mysqli->connect_errno})<br/>{$this->mysqli->connect_error}"
            );
        }
    }
    
    public function create()
    {

    }

    /**
     * Добавление полей к запросу
     * @param  string $fields Поля таблиц
     * @return string
     */
    public function select($column = null)
    {
        $column = explode(',', $column); //Разбиваем строку

        foreach ($column as  $value) {              //проходимся в цикле для удаление пробелов и добавление кавычек
            $this->select_column .= "`".trim($value)."`, ";
        }

        $this->select_column = substr_replace($this->select_column, ' ', -2); //Удаляем запятую в конце строки
    }

    /**
     * Выборка всех записей из указанной таблицы
     * @param  string  $name_table  Имя таблицы бд
     * @param  int $limit_start Начальная позиция строки для выборки
     * @param  int $limit_max   Максимальное количество возвращаемых строк
     * @return string           Строка запроса
     */
    public function get_all($name_table, $limit_start = false, $limit_max = false)
    {
        /**
         * Проверяем нужно добавить в запрос поля для выборки
         */
        if (empty($this->select_column)) 
            $sql = "SELECT * FROM `".$name_table."` ";
        else
            $sql = "SELECT ".$this->select_column." FROM `".$name_table."` ";

        /**
         * Если $limit_start не false добавляем параметр к запросу
         */
        if ($limit_start != false) {
            $sql .= "LIMIT ".$limit_start." ";
        }

        if ($limit_start != false AND $limit_max != false) {
            $sql .= "LIMIT ".$limit_start.",".$limit_max." ";
        }
        return $this->query($sql);
    }

    /**
     * Медод для вставки запроса в базу данных
     * @param  string $sql Строка запроса
     * @return object
     */
    public function query($sql)
    {
        throw new \Exception($sql);
        /**
         * Если запрос пустой, выводим ошибку
         */
        if (empty($sql)) {
            throw new \Exception(Liw::$lang['empty_sql']);
        }

        /**
         * Отправка запроса в бд
         * если есть ошибки, выводим
         * @var string
         */
        if (!$result = $this->mysqli->query($sql))
            throw new \Exception($this->mysqli->error);

        if (is_object($result))
            return $result;
    }
}
