<?php
namespace liw\core\helper;

/**
 * Класс обработки ошибок
 */

class ErrorHandler
{   
    public function myErrorHandler($errno, $errstr, $file, $line)
    {   
        throw new \Exception($errstr . '<hr>Файл: ' . $file . '<hr>Строка: ' . $line);
    }

    public function error()
    {
        set_error_handler([$this, 'myErrorHandler']);
    }

}