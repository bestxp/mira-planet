<?php
namespace liw\views;

use liw\core\Liw;
use liw\core\base\View;

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?=Liw::$config['charset']?>"/>
    <link rel="SHORTCUT ICON" href="/img/ico/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" media="screen,projection"
          href="/css/site.css" />
    <title><?=$this->title?></title>
</head>
<body>

<div class="wrap">
    <div id = "left_menu_wrap"></div>

    <div id= "left_menu">
        <h1> <a href = "/" id="logo"><?=Liw::$config['site_name']?></a></h1>

        <ol id="l_m">
            <form action="/user/login" method="post">
                <input type="text" name = "login"
                       placeholder="login"/>

                <input type="password" name = "password"
                       placeholder="password" />

                <input type="submit" name = "login_submit"
                       value = "Войти" />
                <a href="/user/resset_pass" class = "lost_pass" >Забыли пароль?</a>
            </form>
            <li><a href="/user/registration"> Регистрация</a></li>
            <li><a href="/user/index"> Статьи</a></li>


            <li class="logo_boot">&copy; MIRA 2015</li>

        </ol>
    </div>

    <div id="main">
        <?=$this->view;?>
    </div>
</div>

<div class="footer">
    <div>

    </div>

    <div class="develop">
        <?php echo '<p>Код выполнен за '.number_format(microtime(1)-TIME,3).' секунды</p>';?>
    </div>

</div>

<div class="js-block">
    <script src="/js/jquery-1.11.1.min.js" type="text/javascript" ></script>
    <script src="/js/js.js" type="text/javascript" ></script>
</div>


</body>
</html>