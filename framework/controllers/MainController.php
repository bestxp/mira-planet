<?php
namespace liw\controllers;

use liw\core\base\Controller;
use liw\core\Liw;

class MainController extends Controller
{
    public function index()
    {
        $this->view->show('index');
    }
   
}
