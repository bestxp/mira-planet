<?php
define("TIME", microtime(1));

error_reporting(-1);
//set_time_limit(10);

require __DIR__ . '/../framework/bootstrap.php'; //Загрузка приложения

$config =  require PATH . '/config/config.php'; //Подключаем файл конфига

$lang = require PATH . '/config/lang_ru.php'; //Загрузка языка

/**
 * Запуск приложения
 *
 * @var $app object liw\core\base\Router
 */
$app->start($config, $lang);


